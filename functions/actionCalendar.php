<?php 
	include_once("./koneksi_db.php");
	session_start();
	$root = BASE_URL;

	if(isset($_POST['get-calendar'])) {

		$fd 	= $_POST['firstday'];
		$dim	= $_POST['dayinmonth'];
		$bd		= $_POST['boxdisplay'];
		$tdd 	= $_POST['totaldaysdisplay'];
		$month 	= $_POST['month']+1;
		$year 	= $_POST['year'];
		$tDate	= $_POST['todayDate'];
		$tMonth	= $_POST['todayMonth']+1;
		$tYear	= $_POST['todayYear'];

		$date = 1;
		echo "<ul>";
		for($i = 1; $i <= $bd; $i++){
			if(($i >= $fd + 1 || $fd == 7) && $i <= ($tdd)){
				$eventNum = 0;
				$getQry  = "SELECT pasien_id FROM pasien WHERE DAY(born_date) = :day AND MONTH(born_date) = :month";
				$dataPasien  = $pdo->prepare($getQry);
				$dataPasien->bindValue(":day", $date, PDO::PARAM_STR);
				$dataPasien->bindValue(":month", $month, PDO::PARAM_STR);
				$dataPasien->execute();
				//echo $dataPasien->rowCount();
				if ($dataPasien->rowCount() < 1) {
					$eventNum = 0;
				} else {
					$eventNum = $dataPasien->fetchAll(PDO::FETCH_ASSOC);
				}

				if ($date == $tDate && $year == $tYear && $month == $tMonth && $eventNum < 1) {
					echo "<li class='grey date_cell'>";
				} elseif ($eventNum > 0) {
					$varID = array();
					foreach ($eventNum as $row) {
						$varID[] = $row['pasien_id'];
					}
					$ID = implode("a", $varID);
					$modalDate = $date.".".$month;
					$a = "tes string";
					echo '<li class="light_sky date_cell li-'.$date.'" data-rowid="'.$ID.'" onclick="showModalDetail('.$modalDate.')">';
				} else {
					echo "<li class='date_cell'>";
				}
				echo $date;
				echo "</li>";
				$date++;
			} else {
				echo "<li class='date_cell'></li>";
			}
		}
		echo "</ul>";
	}

	elseif(isset($_POST['get-sub-calendar'])) {
		$id = explode("a", $_POST['id']);
		$date = $_POST['day']." ".$_POST['month'];
		echo '
			<div class="item"><i class="clock outline icon"></i>
				<div class="content">
					<div id="modal-header" class="header">'.$date.'</div>
		';

		foreach ($id as $rowid) {
			$getQry  = "SELECT firstname, lastname, gender FROM pasien WHERE pasien_id = :rowid";
			$data  = $pdo->prepare($getQry);
			$data->bindValue(":rowid", $rowid, PDO::PARAM_STR);
			$data->execute();
			$data = $data->fetchAll(PDO::FETCH_ASSOC);

			if ($data[0]['gender'] == "L") {
				$gender = "Mr.";
			} else {
				$gender = "Ms.";
			}

			$data = $gender." ".$data[0]['firstname']." ".$data[0]['lastname'];

			echo '
			<div class="ui grid">
				<div class="description eight wide column" style="text-transform: capitalize;">'.$data.'</div>
				<div class="eight wide column"><button class="mini ui red button" onclick="showModalProfil('.$rowid.')">Lihat Profil</button></div>
			</div>
			';	
		}
		echo '</div></div>';
	}

	elseif(isset($_POST['get-patient-profil'])) {
		$pasid = $_POST['pasid'];
		$getQry  = "SELECT * FROM pasien WHERE pasien_id = :pasid";
		$dataPatient  = $pdo->prepare($getQry);
		$dataPatient->bindValue(":pasid", $pasid, PDO::PARAM_STR);
		$dataPatient->execute();

		if ($dataPatient->rowCount() < 1) {
			echo "<i>data tidak ditemukan</i>";
		} else {
			$data = $dataPatient->fetchAll(PDO::FETCH_ASSOC);

			$fname = $data[0]['firstname'];
			$lname = $data[0]['lastname'];
			$bplace = $data[0]['born_place'];
			$bdate = $data[0]['born_date'];
			$contact = $data[0]['contact'];
			$address = $data[0]['address'];
			$gender = $data[0]['gender'];
			$treatment = $data[0]['treatment'];
			$age = date("Y") - date("Y", strtotime($data[0]['born_date']));

			if ($gender == "L") {
				$gRender = "Pria";
			} else {
				$gRender = "Wanita";
			}

			echo '<br/>
				<div class="ui form otdc login" style="text-transform: capitalize !important">
	              <div class="two fields">
	                <div class="field">
	                  <label for="">Nama Depan</label>
	                  <div class="ui transparent input">
	                    <label>'.$fname.'</label>
	                  </div>
	                </div>
	                <div class="field">
	                  <label for="">Nama Belakang</label>
	                  <div class="ui transparent input">
	                    <label>'.$lname.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="two fields">
	                <div class="field">
	                  <label for="">Tempat Lahir</label>
	                  <div class="ui transparent input">
	                    <label>'.$bplace.'</label>
	                  </div>
	                </div>
	                <div class="field">
	                  <label for="">Tanggal Lahir</label>
	                  <div class="ui transparent input">
	                    <label>'.$bdate.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="two fields">
	                <div class="field">
	                  <label for="">No. HP</label>
	                  <div class="ui transparent input">
	                    <label>'.$contact.'</label>
	                  </div>
	                </div>
	                <div class="field">
	                  <label for="">Jenis Kelamin</label>
	                  <div class="ui transparent input">
	                    <label>'.$gRender.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="two fields">
		              <div class="field">
		                <label for="">Alamat</label>
		                <div class="ui transparent input">
		                  <label>'.$address.'</label>
		                </div>
		              </div>
		              <div class="field">
	                  <label for="">Usia</label>
	                  <div class="ui transparent input">
	                    <label>'.$age.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="field">
	                <label for="">Treatment</label>
	                <div class="ui transparent input">
	                  <label>'.$treatment.'</label>
	                </div>
	              </div><br/>
	            </div>
			';
		}
	}

	else {
		echo "get data failed";
	}
?>