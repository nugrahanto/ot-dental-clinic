<?php
	include_once("./koneksi_db.php");
	session_start();
	$root = BASE_URL;

	//ADD PATIENT
	if(isset($_POST['greetings-add'])) {
		$user 		= $_SESSION["access-login"];
		$greetings 	= $_POST['greetings'];

		// begin transaction
		$pdo->beginTransaction();
		try {
			$addQry = "INSERT INTO greetings (greetings, create_by, modified_by) VALUES (:greetings, :user, :user)";
			$addPasien = $pdo->prepare($addQry);
			$addPasien->bindValue(":greetings", $greetings, PDO::PARAM_STR);
			$addPasien->bindValue(":user", $user, PDO::PARAM_STR);
			$addPasien->bindValue(":user", $user, PDO::PARAM_STR);
			$addPasien->execute();
			$pdo->commit();
			header('location: '.$root.'greetings.php');
		} catch (PDOException $e) {
			$pdo->rollback();
			header('location: '.$root.'greetings.php?err=failed');
		}
	}

	//EDIT PATIENT
	if(isset($_POST['greetings-put'])) {
		$user 		= $_SESSION["access-login"];
		$id 		= $_POST['id'];
		$greetings 	= $_POST['greetings'];

		// begin transaction
		$pdo->beginTransaction();
		try {
			$putQry = "UPDATE greetings SET `greetings`=:greetings, `modified_by`=:user WHERE `greetings_id`=:id";
			$putGreetings = $pdo->prepare($putQry);
			$putGreetings->bindValue(":greetings", $greetings, PDO::PARAM_STR);
			$putGreetings->bindValue(":user", $user, PDO::PARAM_STR);
			$putGreetings->bindValue(":id", $id, PDO::PARAM_STR);
			$putGreetings->execute();

			$pdo->commit();
			header('location: '.$root.'greetings.php');
		} catch (PDOException $e) {
			$pdo->rollback();
			header('location: '.$root.'greetings.php?err=failed');
		}
	}

	//DELETE PATIENT
	if(isset($_GET['greetings-del'])) {
		$rowid = $_GET['greetings-del'];
		$dellQry  = "DELETE FROM greetings WHERE greetings_id = :rowid";
		$dellGreetings  = $pdo->prepare($dellQry);
		$dellGreetings->bindValue(":rowid", $rowid, PDO::PARAM_STR);
		$dellGreetings->execute();

		header('location: '.$root.'greetings.php');
	}

	//AJAX MODAL DELETE
	if(isset($_POST['g-dell-modal'])) {
		$rowid = $_POST['rowid'];

		$getQry  = "SELECT * FROM greetings WHERE greetings_id = :rowid";
		$dataGreetings  = $pdo->prepare($getQry);
		$dataGreetings->bindValue(":rowid", $rowid, PDO::PARAM_STR);
		$dataGreetings->execute();

		if ($dataGreetings->rowCount() < 1) {
			$data = "0";
		} else {
			$gCount	= $dataGreetings->rowCount();
			$data = $dataGreetings->fetchAll(PDO::FETCH_ASSOC);
		}

		echo '
			<div class="ui red basic cancel button"><i class="remove icon"></i><span>Tidak</span></div>
      		<a href = "'.$root.'functions/actionGreetings.php?greetings-del='.$data[0]['greetings_id'].'">
      			<div class="ui blue basic ok button"><i class="checkmark icon"></i><span>Iya</span></div>
      		</a>
		';
	}

	//AJAX MODAL EDIT
	if(isset($_POST['g-edit-modal'])) {
		$rowid = $_POST['rowid'];

		$getQry  = "SELECT * FROM greetings WHERE greetings_id = :rowid";
		$dataGreetings  = $pdo->prepare($getQry);
		$dataGreetings->bindValue(":rowid", $rowid, PDO::PARAM_STR);
		$dataGreetings->execute();

		if ($dataGreetings->rowCount() < 1) {
			$data = "0";
		} else {
			$gCount	= $dataGreetings->rowCount();
			$data = $dataGreetings->fetchAll(PDO::FETCH_ASSOC);

			echo '
				<form class="ui form otdc login" method="post" id="form-greetings-put" action="'.$root.'functions/actionGreetings.php">
				  <input type="hidden" name="id" value="'.$rowid.'" required />
	          	  <div class="fields">
	          	  	<div class="required field" style="width: 100%;">
	          	  	  <label for="">Kalimat Ucapan</label>
	          	  	  <div class="ui transparent input">
	                    <textarea name="greetings" >'.$data[0]['greetings'].'</textarea>
	              	  </div>
	              	</div>
	          	  </div>
	          	</form>

	          	<button type="submit" form="form-greetings-put" name="greetings-put" value="Simpan" class="ui animated blue button">
	              <div class="visible content">Simpan</div>
	              <div class="hidden content"><i class="paper plane outline icon"></i></div>
	          	</button>
			';
		}
	}
?>