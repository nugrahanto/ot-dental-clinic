<?php
	include_once("./koneksi_db.php");
	session_start();
	$root = BASE_URL;

	//LOGIN
	if(isset($_POST['user-login'])) {
		$user 		= $_POST['user'];
		$password 	= md5($_POST['pass']);

		//check user existed or not
		$qry = "SELECT username, password, status FROM user WHERE username=:user";
		$userCheck = $pdo->prepare($qry);
		$userCheck->bindValue(":user", $user, PDO::PARAM_STR);
		$userCheck->execute();

		if ($userCheck->rowCount() > 0) {
			$dbData = $userCheck->fetchAll(PDO::FETCH_ASSOC);
			$dbUser = $dbData[0]['username'];
			$dbPass = $dbData[0]['password'];
			$dbStat = $dbData[0]['status'];

			if ($password == $dbPass) {
				if ($dbStat == ""/*ad your admin level*/ OR $dbStat == ""/*ad your admin level*/) {
					$_SESSION["userauth-for-admin_token-key"] = ''/*ad your auth token private key*/;
					$_SESSION["access-login"] = $dbUser;

					echo "success";
				} else {
					echo "your account is notactive, please contact your admin or wait until your account is active";
				}
			} else {
				echo "invalid password";
			}
		}
		else {
			echo "user notfound";
		}
	}

	//SIGNUP
	elseif(isset($_POST['user-signup'])) {
		$firstname 	= $_POST['firstname'];
		$lastname 	= $_POST['lastname'];
		$username	= $_POST['username'];
		$password 	= md5($_POST['password']);

		$chckUserQry= "SELECT username FROM user WHERE username = :username";
		$chckUser 	= $pdo->prepare($chckUserQry);
		$chckUser->bindValue(":username", $username, PDO::PARAM_STR);
		$chckUser->execute();

		if ($chckUser->rowCount() > 0) {
			header('location: '.$root.'signup.php?alert=user');
		} else {
			// begin transaction
			$pdo->beginTransaction();
			try {
				$addQry = "INSERT INTO user (username, firstname, lastname, password) VALUES (:username, :firstname, :lastname, :password) ";
				$addUser = $pdo->prepare($addQry);
				$addUser->bindValue(":username", $username, PDO::PARAM_STR);
				$addUser->bindValue(":firstname", $firstname, PDO::PARAM_STR);
				$addUser->bindValue(":lastname", $lastname, PDO::PARAM_STR);
				$addUser->bindValue(":password", $password, PDO::PARAM_STR);
				$addUser->execute();
				$pdo->commit();
				header('location: '.$root.'?alert=done');
			} catch (PDOException $e) {
				$pdo->rollback();
				header('location: '.$root.'404.php');
			}
		}
	}

	//LOGOUT
	elseif(isset($_POST['user-logout'])) {
		session_destroy();
		$_POST = array();
		header('Refresh:0');
	}

	else {
		header('location: '.$root);
	}
?>