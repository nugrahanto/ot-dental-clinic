<?php 
include_once("./koneksi_db.php"); 

$getQry = "SELECT *, DATE_FORMAT(born_date,'%Y-%m-%d') as born_date, YEAR(DATE_FORMAT(CURDATE(),'%Y-%m-%d')) - YEAR(born_date) as age FROM pasien WHERE (MONTH(born_date)=MONTH(DATE_FORMAT(CURDATE(),'%Y-%m-%d')) AND DAY(born_date)=DAY(DATE_FORMAT(CURDATE(),'%Y-%m-%d'))) OR (DAY(LAST_DAY(born_date))=29 AND DAY(born_date)=29 AND DAY(LAST_DAY(DATE_FORMAT(CURDATE(),'%Y-%m-%d')))=28)";

$patientDay  = $pdo->prepare($getQry);
$patientDay->execute();

if ($patientDay->rowCount() > 0) {
	$rowCount = $patientDay->rowCount();
	$data = $patientDay->fetchAll(PDO::FETCH_ASSOC);

	for ($i=0; $i < $rowCount; $i++) { 
		$pdo->beginTransaction();
		try {
			$addQry = "INSERT INTO bdhistory(pasien_id, firstname, lastname, address, born_date, age, gender, contact) VALUES (:pasienid, :firstname, :lastname, :address, :bdate, :age, :gender, :contact)";
			$addUser = $pdo->prepare($addQry);
			$addUser->bindValue(":pasienid", $data[$i]['pasien_id'], PDO::PARAM_STR);
			$addUser->bindValue(":firstname", $data[$i]['firstname'], PDO::PARAM_STR);
			$addUser->bindValue(":lastname", $data[$i]['lastname'], PDO::PARAM_STR);
			$addUser->bindValue(":address", $data[$i]['address'], PDO::PARAM_STR);
			$addUser->bindValue(":bdate", $data[$i]['born_date'], PDO::PARAM_STR);
			$addUser->bindValue(":age", $data[$i]['age'], PDO::PARAM_STR);
			$addUser->bindValue(":gender", $data[$i]['gender'], PDO::PARAM_STR);
			$addUser->bindValue(":contact", $data[$i]['contact'], PDO::PARAM_STR);
			$addUser->execute();
			$pdo->commit();
		} catch (PDOException $e) {
			$pdo->rollback();
		}
	}
}
?>