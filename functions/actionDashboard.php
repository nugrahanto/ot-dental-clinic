<?php 
	include_once("./koneksi_db.php");
	$todayDate = date("d");
	$todayMonth =  date("m");

	if (isset($_POST['d-get-notif-count'])) {
		$getQry  = "SELECT COUNT(bd_id) AS NumberOfBirthday FROM bdhistory WHERE DAY(born_date) = :todayDate AND MONTH(born_date) = :todayMonth AND status = '0'";
		$bdCount = $pdo->prepare($getQry);
		$bdCount->bindValue(":todayDate", $todayDate, PDO::PARAM_STR);
		$bdCount->bindValue(":todayMonth", $todayMonth, PDO::PARAM_STR);
		$bdCount->execute();

		if ($bdCount->rowCount() < 1) {
			echo '<span class="otdc label-nomessage">0</span>';
		} else { 
			$bdCount = $bdCount->fetchAll(PDO::FETCH_ASSOC);
			if ($bdCount[0]['NumberOfBirthday'] == 0) {
				echo '<span class="otdc label-nomessage">'.$bdCount[0]['NumberOfBirthday'].'</span>';
			} else {
				echo '<span class="otdc label-message">'.$bdCount[0]['NumberOfBirthday'].'</span>';
			}
		}
	}

	elseif (isset($_POST['d-get-notif'])) {
		$getQry  = "SELECT firstname, lastname FROM bdhistory WHERE DAY(born_date) = :todayDate AND MONTH(born_date) = :todayMonth";
		$bdData = $pdo->prepare($getQry);
		$bdData->bindValue(":todayDate", $todayDate, PDO::PARAM_STR);
		$bdData->bindValue(":todayMonth", $todayMonth, PDO::PARAM_STR);
		$bdData->execute();

		if ($bdData->rowCount() < 1) {
			echo '
			<div class="ui segment">
				<p style="color: grey !important;"><i>tidak ada notifikasi</i></p>
			</div>
			<a href="notification.php"><div class="ui center aligned segment active">
				<p>Semua Notifikasi</p>
			</div></a>
			';
		} else {
			$bdData = $bdData->fetchAll(PDO::FETCH_ASSOC);
			foreach ($bdData as $row) {
				$firstname = $row['firstname'];
				$lastname = $row['lastname'];
				echo '
				<div class="ui segment">
					<p>'.$firstname.' '.$lastname.'</p>
				</div>
				';
			}
			echo '
			<a href="notification.php"><div class="ui center aligned segment active">
				<p>Semua Notifikasi</p>
			</div></a>
			';
		}
	}
 ?>