<?php
	include_once("./koneksi_db.php");
	session_start();
	$root = BASE_URL;

	//ADD PATIENT
	if(isset($_POST['pasien-add'])) {
		$user 			= $_SESSION["access-login"];
		$firstname 		= $_POST['firstname'];
		$lastname 		= $_POST['lastname'];
		$bplace 		= $_POST['bplace'];
		$bdate	 		= $_POST['bdate'];
		$contact 		= $_POST['contact'];
		$gender 		= $_POST['gender'];
		$address 		= $_POST['address'];
		if (isset($_POST['treatment'])) {
			$tString= implode(", ",$_POST['treatment']);
		} else {
			$tString = "";
		}

		// begin transaction
		$pdo->beginTransaction();
		try {
			$addQry = "INSERT INTO pasien (contact, firstname, lastname, address, gender, born_place, born_date, treatment, create_by, modified_by) VALUES (:contact, :firstname, :lastname, :address, :gender, :bplace, :bdate, :tString, :user, :user)";
			$addPasien = $pdo->prepare($addQry);
			$addPasien->bindValue(":contact", $contact, PDO::PARAM_STR);
			$addPasien->bindValue(":firstname", $firstname, PDO::PARAM_STR);
			$addPasien->bindValue(":lastname", $lastname, PDO::PARAM_STR);
			$addPasien->bindValue(":address", $address, PDO::PARAM_STR);
			$addPasien->bindValue(":gender", $gender, PDO::PARAM_STR);
			$addPasien->bindValue(":bplace", $bplace, PDO::PARAM_STR);
			$addPasien->bindValue(":bdate", $bdate, PDO::PARAM_STR);
			$addPasien->bindValue(":tString", $tString, PDO::PARAM_STR);
			$addPasien->bindValue(":user", $user, PDO::PARAM_STR);
			$addPasien->bindValue(":user", $user, PDO::PARAM_STR);
			$addPasien->execute();
			$pdo->commit();
			header('location: '.$root.'patient.php');
		} catch (PDOException $e) {
			$pdo->rollback();
			header('location: '.$root.'patient.php?err=failed');
		}
	}

	//DELETE PATIENT DATA
	if(isset($_GET['pasien-del'])) {
		$rowid = $_GET['pasien-del'];
		$dellQry  = "DELETE FROM pasien WHERE pasien_id = :rowid";
		$dellPatient  = $pdo->prepare($dellQry);
		$dellPatient->bindValue(":rowid", $rowid, PDO::PARAM_STR);
		$dellPatient->execute();

		header('location: '.$root.'patient.php');
	}

	//UPDATE PATIENT DATA
	if(isset($_POST['pasien-put'])) {
		$user 		= $_SESSION["access-login"];
		$id 		= $_POST['id'];
		$firstname 	= $_POST['firstname'];
		$lastname 	= $_POST['lastname'];
		$bplace 	= $_POST['bplace'];
		$bdate	 	= $_POST['bdate'];
		$contact 	= $_POST['contact'];
		$gender 	= $_POST['gender'];
		$address 	= $_POST['address'];

		if (isset($_POST['treatment'])) {
			$tString= implode(", ",$_POST['treatment']);
		} else {
			$tString = "";
		}

		// begin transaction
		$pdo->beginTransaction();
		try {
			$putQry = "UPDATE pasien SET `contact`=:contact, `firstname`=:firstname, `lastname`=:lastname, `address`=:address, `gender`=:gender, `born_place`=:bplace, `born_date`=:bdate, `treatment`=:tString, `modified_by`=:user WHERE `pasien_id`=:id";
			$putPatient = $pdo->prepare($putQry);
			$putPatient->bindValue(":contact", $contact, PDO::PARAM_STR);
			$putPatient->bindValue(":firstname", $firstname, PDO::PARAM_STR);
			$putPatient->bindValue(":lastname", $lastname, PDO::PARAM_STR);
			$putPatient->bindValue(":address", $address, PDO::PARAM_STR);
			$putPatient->bindValue(":gender", $gender, PDO::PARAM_STR);
			$putPatient->bindValue(":bplace", $bplace, PDO::PARAM_STR);
			$putPatient->bindValue(":bdate", $bdate, PDO::PARAM_STR);
			$putPatient->bindValue(":tString", $tString, PDO::PARAM_STR);
			$putPatient->bindValue(":id", $id, PDO::PARAM_STR);
			$putPatient->bindValue(":user", $user, PDO::PARAM_STR);
			$putPatient->execute();

			$pdo->commit();
			header('location: '.$root.'patient.php');
		} catch (PDOException $e) {
			$pdo->rollback();
			header('location: '.$root.'patient.php?err=failed');
		}
	}

	//GET PATIENT BY ID FOR MODAL DELETE 
	if(isset($_POST['p-dell-modal'])) {
		$rowid = $_POST['rowid'];

		$getQry  = "SELECT * FROM pasien WHERE pasien_id = :rowid";
		$dataPatient  = $pdo->prepare($getQry);
		$dataPatient->bindValue(":rowid", $rowid, PDO::PARAM_STR);
		$dataPatient->execute();

		$data = $dataPatient->fetchAll(PDO::FETCH_ASSOC);

		echo '
			<div class="ui icon header"><i class="trash icon"></i>
				<span>ATTENTION!!<br/>Anda akan menghapus biodata<br/><br/>
				<i style="text-transform:capitalize;">'.$data[0]["firstname"].' '.$data[0]['lastname'].'</i></span>
			</div>
			<div class="content">
				<div class="description">Data yang sudah dihapus tidak dapat dikembalikan lagi atau dengan kata lain terhapus secara permanen. Apakah Anda yakin ingin menghapus data ini?</div>
      		</div>
      		<div class="actions">
      			<div class="ui red basic cancel button"><i class="remove icon"></i><span>Tidak</span></div>
      			<a href = "'.$root.'functions/actionPatient.php?pasien-del='.$data[0]['pasien_id'].'"><div class="ui blue basic ok button"><i class="checkmark icon"></i><span>Iya</span></div></a>
      		</div>
		';
	}

	//GET PATIENT BY ID FOR MODAL EDIT 
	if(isset($_POST['p-edit-modal'])) {
		$rowid = $_POST['rowid'];

		$getQry  = "SELECT * FROM pasien WHERE pasien_id = :rowid";
		$dataPatient  = $pdo->prepare($getQry);
		$dataPatient->bindValue(":rowid", $rowid, PDO::PARAM_STR);
		$dataPatient->execute();

		$data = $dataPatient->fetchAll(PDO::FETCH_ASSOC);

		$fname = $data[0]['firstname'];
		$lname = $data[0]['lastname'];
		$bplace = $data[0]['born_place'];
		$bdate = $data[0]['born_date'];
		$contact = $data[0]['contact'];
		$address = $data[0]['address'];
		$gender = $data[0]['gender'];
		$treatment = explode(", ", $data[0]['treatment']);

		$trmt_chckbox_1 = in_array('Bleaching', $treatment) ? 'checked':' ';
		$trmt_chckbox_2 = in_array('Gigi Tiruan', $treatment) ? 'checked':' ';
		$trmt_chckbox_3 = in_array('Konsultasi', $treatment) ? 'checked':' ';
		$trmt_chckbox_4 = in_array('Odontectomy', $treatment) ? 'checked':' ';
		$trmt_chckbox_5 = in_array('Orthondontyl', $treatment) ? 'checked':' ';
		$trmt_chckbox_6 = in_array('Pencabutan', $treatment) ? 'checked':' ';
		$trmt_chckbox_7 = in_array('Perawatan', $treatment) ? 'checked':' ';
		$trmt_chckbox_8 = in_array('Preventif', $treatment) ? 'checked':' ';
		$trmt_chckbox_9 = in_array('Scalling', $treatment) ? 'checked':' ';
		$trmt_chckbox_10 = in_array('Tambal', $treatment) ? 'checked':' ';

		if ($gender == "L") {
			$gRender = 
				'<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="gender" value="L" checked="checked" />
						<label>L</label>
					</div>
				</div>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="gender" value="P" />
						<label>P</label>
					</div>
				</div>';
		} elseif ($gender == "P") {
			$gRender = 
				'<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="gender" value="L" />
						<label>L</label>
					</div>
				</div>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="gender" value="P" checked="checked" />
						<label>P</label>
					</div>
				</div>';
		}

		echo '<br/>
			<form class="ui form otdc login" method="post" id="form-pasien-put" action="'.$root.'functions/actionPatient.php">
			  <input type="hidden" name="id" value="'.$rowid.'" required />
              <div class="two fields">
                <div class="required field">
                  <label for="">Nama Depan</label>
                  <div class="ui transparent input">
                    <input type="text" name="firstname" value="'.$fname.'" required />
                  </div>
                </div>
                <div class="field">
                  <label for="">Nama Belakang</label>
                  <div class="ui transparent input">
                    <input type="text" name="lastname" value="'.$lname.'" />
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="required field">
                  <label for="">Tempat Lahir</label>
                  <div class="ui transparent input">
                    <input type="text" name="bplace" value="'.$bplace.'" required />
                  </div>
                </div>
                <div class="required field">
                  <label for="">Tanggal Lahir</label>
                  <div class="ui transparent input">
                    <input type="date" name="bdate" value="'.$bdate.'" required />
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <label for="">No. HP</label>
                  <div class="ui transparent input">
                    <input type="number" name="contact" value="'.$contact.'" />
                  </div>
                </div>
                <div class="required field">
                  <label for="">Jenis Kelamin</label>
                  <div class="inline fields">
                    '.$gRender.'
                  </div>
                </div>
              </div>
              <div class="required field">
                <label for="">Alamat</label>
                <div class="ui transparent input">
                  <input type="text" name="address" value="'.$address.'" />
                </div>
              </div>
              <div class="field">
                <label for="">Treatment</label>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Bleaching" '.$trmt_chckbox_1.'/>
                    <label>Bleaching</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Gigi Tiruan" '.$trmt_chckbox_2.'/>
                    <label>Gigi Tiruan</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Konsultasi" '.$trmt_chckbox_3.'/>
                    <label>Konsultasi</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Odontectomy" '.$trmt_chckbox_4.'/>
                    <label>Odontectomy</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Orthondontyl" '.$trmt_chckbox_5.'/>
                    <label>Orthondontyl</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Pencabutan" '.$trmt_chckbox_6.'/>
                    <label>Pencabutan</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Perawatan" '.$trmt_chckbox_7.'/>
                    <label>Perawatan</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Preventif" '.$trmt_chckbox_8.'/>
                    <label>Preventif</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Scalling" '.$trmt_chckbox_9.'/>
                    <label>Scalling</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Tambal" '.$trmt_chckbox_10.'/>
                    <label>Tambal</label>
                  </div>
                </div>
              </div><br/>
            </form>
            <button type="submit" form="form-pasien-put" name="pasien-put" value="Simpan" class="ui animated blue button">
              <div class="visible content">Simpan</div>
              <div class="hidden content"><i class="paper plane outline icon"></i></div>
          	</button>
		';
	}
?>