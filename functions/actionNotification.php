<?php 
	include_once("./koneksi_db.php");
	session_start();
	$root = BASE_URL;

	//GET DATA TABLE
	if(isset($_POST['n-get-sidebar-data'])) {
		$getQry  = "SELECT * FROM bdhistory ORDER BY create_date DESC LIMIT 3";
		$dataNotif  = $pdo->prepare($getQry);
		$dataNotif->execute();
		if ($dataNotif->rowCount() < 1) {
			echo ' ';
		} else {
			$dataNotif = $dataNotif->fetchAll(PDO::FETCH_ASSOC);
			foreach ($dataNotif as $data) {
				$day = date("d", strtotime($data['born_date']));
				$month = date("n", strtotime($data['born_date']));
				$year = date("Y");
				$months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

				if ($day == date("d") && $month == date("n")) {
					$date = "today";
				} else {
					$date = $day.", ".$months[$month-1]." ".$year;
				}
				echo '
				<div class="ui segment otdc item-notification">
					<div class="ui grid">
						<div class="sixteen wide mobile four wide tablet four wide computer four wide large screen center aligned column otdc icon-notification"><i class="big gift icon"></i></div>
						<div class="sixteen wide mobile twelve wide tablet twelve wide computer twelve wide large screen column">
							<div class="ui list">
								<div class="item"><i class="clock outline icon"></i>
									<div class="content">
										<div class="header">'.$date.'</div>
										<div class="description">'.$data['firstname'].' '.$data['lastname'].'</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				';
			}
		}
	}

	//GET PATIENT PROFIL
	if(isset($_POST['n-patient-profil'])) {
		$pasid = $_POST['pasid'];
		$age = $_POST['age'];

		$getQry  = "SELECT * FROM pasien WHERE pasien_id = :pasid";
		$dataPatient  = $pdo->prepare($getQry);
		$dataPatient->bindValue(":pasid", $pasid, PDO::PARAM_STR);
		$dataPatient->execute();

		if ($dataPatient->rowCount() < 1) {
			echo "<i>data tidak ditemukan</i>";
		} else {
			$data = $dataPatient->fetchAll(PDO::FETCH_ASSOC);

			$fname = $data[0]['firstname'];
			$lname = $data[0]['lastname'];
			$bplace = $data[0]['born_place'];
			$bdate = $data[0]['born_date'];
			$contact = $data[0]['contact'];
			$address = $data[0]['address'];
			$gender = $data[0]['gender'];
			$treatment = $data[0]['treatment'];

			if ($gender == "L") {
				$gRender = "Pria";
			} else {
				$gRender = "Wanita";
			}

			echo '<br/>
				<div class="ui form otdc login" style="text-transform: capitalize !important">
	              <div class="two fields">
	                <div class="field">
	                  <label for="">Nama Depan</label>
	                  <div class="ui transparent input">
	                    <label>'.$fname.'</label>
	                  </div>
	                </div>
	                <div class="field">
	                  <label for="">Nama Belakang</label>
	                  <div class="ui transparent input">
	                    <label>'.$lname.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="two fields">
	                <div class="field">
	                  <label for="">Tempat Lahir</label>
	                  <div class="ui transparent input">
	                    <label>'.$bplace.'</label>
	                  </div>
	                </div>
	                <div class="field">
	                  <label for="">Tanggal Lahir</label>
	                  <div class="ui transparent input">
	                    <label>'.$bdate.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="two fields">
	                <div class="field">
	                  <label for="">No. HP</label>
	                  <div class="ui transparent input">
	                    <label>'.$contact.'</label>
	                  </div>
	                </div>
	                <div class="field">
	                  <label for="">Jenis Kelamin</label>
	                  <div class="ui transparent input">
	                    <label>'.$gRender.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="two fields">
		              <div class="field">
		                <label for="">Alamat</label>
		                <div class="ui transparent input">
		                  <label>'.$address.'</label>
		                </div>
		              </div>
		              <div class="field">
	                  <label for="">Usia</label>
	                  <div class="ui transparent input">
	                    <label>'.$age.'</label>
	                  </div>
	                </div>
	              </div>
	              <div class="field">
	                <label for="">Treatment</label>
	                <div class="ui transparent input">
	                  <label>'.$treatment.'</label>
	                </div>
	              </div><br/>
	            </div>
			';
		}
	}
?>