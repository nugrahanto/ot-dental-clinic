$(document).ready(function() {
    // Load
    $('.ui.checkbox').checkbox();

    // Setup - add a text input to each footer cell
    $('.dataTable tfoot th.search').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Cari '+title+'" />' );
    } );

    var dataTable = $('.dataTable').DataTable({
        'searching': false,
        'language': {
            'emptyTable': 'Tidak ada data',
            'lengthMenu': 'Tampilkan _MENU_ data',
            'search': 'Pencarian:',
            'info': 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
            'infoEmpty': 'Data tidak ditemukan',
            'infoFiltered': '(dari _MAX_ data)',
            'zeroRecords': 'Pencarian tidak ditemukan',
            'paginate': {
                'first': '<i class="angle double left icon"></i>',
                'last': '<i class="angle double right icon"></i>',
                'next': '<i class="angle right icon"></i>',
                'previous': '<i class="angle left icon"></i>'
            }
        }
    });

    /* Apply the search
    dataTable.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );*/
})