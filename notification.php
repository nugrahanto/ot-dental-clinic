<?php 
	include ('header.php');
	//check auth
	if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') {
		
	} else {
		session_destroy();
		header("location: " . BASE_URL);
	}

	$ntfQry  = "SELECT * FROM bdhistory ORDER BY bd_id DESC";
	$dataNotif  = $pdo->prepare($ntfQry);
	$dataNotif->execute();

	if ($dataNotif->rowCount() < 1) {
		$dataNotif = "0";
	} else {
		$nCount		= $dataNotif->rowCount();
		$dataNotif	= $dataNotif->fetchAll(PDO::FETCH_ASSOC);
	}
?>

	<div id="modalProfil" class="ui small modal">
      <div class="actions">
        <div class="ui grid">
          <div class="twelve wide column" style="text-align: left !important;" >
            <div class="ui header" style="padding-top: inherit; padding-left: inherit; padding-right: inherit; text-transform: uppercase;">Profil Pasien</div>
          </div>
          <div class="four wide column">
            <button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
          </div>
        </div>
      </div>
      <div class="scrolling content">
        <div class="ui grid">
          <div class="four wide column"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
          <div class="twelve wide column">
          	<div class="patient-profil"></div>
          </div>
        </div>
      </div>
    </div>

	<div class="ui centered grid container">
      <div class="sixteen wide column">
        <div class="ui very padded compact segment otdc wrapper"><a href="dashboard.php"><i class="arrow circle left big icon otdc button-back"></i></a>
          <div class="ui grid">
            <div class="sixteen wide column">
              <h3 class="ui header">NOTIFIKASI</h3>
              <div class="ui divider"></div>
              <i style="color: grey;"><span style="color: #db3236 !important;">**</span> ) Font berwarna merah menandakan pasien yang berulang tahun hari ini.</i>
            </div>
            <div id="notification-sidebar" class="sixteen wide tablet five wide computer four wide large screen column">

            </div>
            <div class="sixteen wide tablet eleven wide computer twelve wide large screen column">
              <table class="ui selectable basic small table otdc dataTable" style="cursor: pointer !important; text-transform: capitalize !important;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Ultah ke-</th>
                    <th>P/L</th>
                    <th>No. HP</th>
                  </tr>
                </thead>
                <tbody>
                	<?php 
                		if ($dataNotif != "0") :
                			for ($i=0; $i < $nCount; $i++) :
                				$day = date("d", strtotime($dataNotif[$i]['create_date']));
								$month = date("n", strtotime($dataNotif[$i]['create_date']));
								$year = date("Y", strtotime($dataNotif[$i]['create_date']));

								if ($day == date("d") && $month == date("n") && $year == date("Y")) {
									echo '
									<tr id="otdc-notif-data" onclick="showModalProfil('.$dataNotif[$i]['pasien_id'].', '.$dataNotif[$i]['age'].')" style="color: #db3236 !important" >
									';
								} else {
									echo '
									<tr id="otdc-notif-data" onclick="showModalProfil('.$dataNotif[$i]['pasien_id'].', '.$dataNotif[$i]['age'].')" >
									';
								}
                	?> 
						<td><?php echo $i+1; ?></td>
						<td><?php echo $dataNotif[$i]['firstname'].' '.$dataNotif[$i]['lastname']; ?></td>
						<td><?php echo $dataNotif[$i]['address'] ?></td>
						<td><?php echo $dataNotif[$i]['age'] ?></td>
						<td><?php echo $dataNotif[$i]['gender'] ?></td>
						<td><?php echo $dataNotif[$i]['contact'] ?></td>
					</tr>
					<?php endfor; endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.semanticui.min.js"></script>
    <script src="assets/semantic/semantic.min.js"></script>
    <script src="assets/js/main.min.js"></script>

    <script>
    	function sidebar() {
    		$.ajax({
				type : 'post',
				url : 'functions/actionNotification.php',
				data :  {
                	'n-get-sidebar-data' : 1
                },
                success : function(data){
                	document.getElementById('notification-sidebar').innerHTML = data;
                }
			});
    	}

    	$(document).ready(function() {
    		// Show Sidebar
    		sidebar();
    		// Show Modal Profil
    		showModalProfil = function(id, age) {
    			$('#modalProfil').modal('show');
    			var pasid = id;
    			$.ajax({
    				type : 'post',
    				url : 'functions/actionNotification.php',
    				data :  {
    					'n-patient-profil' : 1,
    					'pasid' : pasid,
    					'age' : age,
    				},
    				success : function(data){
    					$('.patient-profil').html(data);
    				}
    			});
    		}
		})
    </script>

<?php include ('footer.php'); ?>