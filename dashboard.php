<?php 
	include ('header.php');
	//check auth
	if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') {
		
	} else {
		session_destroy();
		header("location: " . BASE_URL);
	}
?>
	<div id="popupNotification" class="otdc popup-notification" style="text-transform: capitalize !important;">
		<div id="notif-content" class="ui raised segments"></div>
    </div>

    <div class="ui right aligned grid" >
	    <form action="functions/actionLogin.php" method="post" id="form-logout" ></form>
    	<div class="sixteen wide column" style="padding-top: 0px !important; padding-bottom: 0px !important;" >
	    	<button id="toggleNotification" class="ui icon button otdc button-notification">
	    		<i class="envelope big icon"></i><span id="notif"></span>
	    	</button>
	    	<button type="submit" form="form-logout" name="user-logout" value="logout" class="ui icon button otdc button-notification" >
				<i class="power off big icon"></i>
			</button>
		</div>
	</div>

	<div class="ui centered grid container" style="margin-top: 0px !important">
		<div class="sixteen wide mobile ten wide tablet eight wide computer seven wide large screen column" style="padding-top: 0px !important;">
			<div class="ui very padded basic segment"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
		</div>
	</div>

	<div class="ui centered grid container">
		<div class="sixteen wide mobile five wide tablet five wide computer five wide large screen center aligned column">
			<a href="patient.php" class="otdc menu"><img src="assets/images/patient.jpg" alt="" class="ui small circular centered image"/></a>
			<h3 class="ui header">Pasien</h3>
		</div>
		<div class="sixteen wide mobile five wide tablet five wide computer five wide large screen center aligned column">
			<a href="calendar.php" class="otdc menu"><img src="assets/images/calendar.jpg" alt="" class="ui small circular centered image"/></a>
			<h3 class="ui header">Kalender</h3>
		</div>
		<div class="sixteen wide mobile five wide tablet five wide computer five wide large screen center aligned column">
			<a href="greetings.php" class="otdc menu"><img src="assets/images/greeting_cards.jpg" alt="" class="ui small circular centered image"/></a>
			<h3 class="ui header">Ucapan</h3>
		</div>
	</div>

	<script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.semanticui.min.js"></script>
	<script src="assets/semantic/semantic.min.js"></script>
	<script src="assets/js/main.min.js"></script>

	<script>
		function countNotification(){
			$.ajax({
				type : 'post',
				url : 'functions/actionDashboard.php',
				data :  {
                	'd-get-notif-count' : 1
                },
                success : function(data){
                	document.getElementById('notif').innerHTML = data;
                }
			});
		}

		function contentNotification(){
			$.ajax({
				type : 'post',
				url : 'functions/actionDashboard.php',
				data :  {
                	'd-get-notif' : 1
                },
                success : function(data){
                	document.getElementById('notif-content').innerHTML = data;
                }
			});
		}

		$(document).ready(function() {
			// Toggle Notification
			$('#toggleNotification').on('click', (function() {
				contentNotification();
				$('#toggleNotification').toggleClass('active');
				$('#popupNotification').toggleClass('active');
			}));

			countNotification();
		})
	</script>
<?php include ('footer.php'); ?>