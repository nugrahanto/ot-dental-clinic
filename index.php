<?php include ('header.php'); ?>
    <div id="login" style="width: 100% !important; height: 100% !important; padding: auto;" onclick="removeActiveClass()">

    <div id="popupNotification" class="otdc popup-notification" >
      <div id="notif-content" class="ui raised segments">
        <div class="ui segment">
          <p style="color: #db3236 !important;"><i class="alert-text"></i></p>
        </div>
      </div>
    </div>

    <div class="ui centered grid container">
      <div class="sixteen wide mobile ten wide tablet eight wide computer seven wide large screen column">
        <div class="ui very padded basic segment"><img src="assets/images/logo.png" alt="" class="ui tiny circular centered image"/>
          <div class="otdc mt50"></div>
          <h4 class="ui center aligned header">LOGIN</h4>
          <form id="form-login" class="ui form otdc login">
            <input type="hidden" name="user-login" value="login" >
            <div class="required field">
              <label for="">Username</label>
              <div class="ui transparent input">
                <input type="text" name="user" required/>
              </div>
            </div>
            <div class="required field">
              <label for="">Password</label>
              <div class="ui transparent input">
                <input type="password" name="pass" required/>
              </div>
            </div>
          </form>
            <div class="ui center aligned basic segment"><a href="signup.php">
                <button type="button" class="ui animated button">
                  <div class="visible content">Sign Up?</div>
                  <div class="hidden content"><i class="user outline icon"></i></div>
                </button></a>
              <button type="submit" form="form-login" class="ui animated red button">
                <div class="visible content">Login</div>
                <div class="hidden content"><i class="right arrow icon"></i></div>
              </button>
            </div>
        </div>
      </div>
    </div>
    </div>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.semanticui.min.js"></script>
    <script src="assets/semantic/semantic.min.js"></script>
    <script src="assets/js/main.min.js"></script>
    <script>
      function removeActiveClass() {

      }

      $( function () {
        $('form').on('submit', function (e) {
          e.preventDefault();
          $.ajax({
            type: 'post',
            url: 'functions/actionLogin.php',
            data: $('form').serialize(),
            success: function (response) {
              console.log(response);
              if(response == "success"){
                window.location.href = "dashboard.php"
              } else {
                $('.alert-text').html(response);
                $('#popupNotification').toggleClass('active');
              }
            }
          });
        });

        // Hide Modal Detail
        removeActiveClass = function() {
          $('#popupNotification').removeClass('active');
        }
      });
    </script>

<?php include ('footer.php'); ?>