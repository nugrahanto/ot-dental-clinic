<?php 
	include ('header.php');
	//check auth
	if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') {
		
	} else {
		session_destroy();
		header("location: " . BASE_URL);
	}

	$grtQRY  = "SELECT * FROM greetings";
	$allGreetings  = $pdo->prepare($grtQRY);
	$allGreetings->execute();

	if ($allGreetings->rowCount() < 1) {
		$dGreetings = "0";
	} else {
		$gCount		= $allGreetings->rowCount();
		$dGreetings	= $allGreetings->fetchAll(PDO::FETCH_ASSOC);
	}
?>
	<div id="modalDetail" class="ui small modal">
      <div class="actions">
        <button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
      </div>
      <div class="content">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore nisi est eaque deleniti maxime fugit, molestias, aspernatur ipsum ab eos ad suscipit quibusdam aliquam ea nihil impedit voluptas optio. Soluta?</p>
      </div>
    </div>

    <div id="modalTambah" class="ui small modal">
      <div class="actions">
      	<div class="ui grid">
      	  <div class="twelve wide column" style="text-align: left !important;" >
      		<div class="ui header" style="padding-top: inherit; padding-left: inherit; padding-right: inherit; text-transform: uppercase;">Tambah Ucapan</div>
      	  </div>
      	  <div class="four wide column">
        	<button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
          </div>
    	</div>
      </div>
      <div class="scrolling content">
        <div class="ui grid">
          <div class="twelve wide column">

          	<form class="ui form otdc login" method="post" id="form-greetings-add" action="functions/actionGreetings.php">
          	  <div class="fields">
          	  	<div class="required field" style="width: 100%;">
          	  	  <label for="">Kalimat Ucapan</label>
          	  	  <div class="ui transparent input">
                    <textarea name="greetings" ></textarea>
              	  </div>
              	</div>
          	  </div>
          	</form>

          	<button type="submit" form="form-greetings-add" name="greetings-add" value="Kirim" class="ui animated blue button">
              <div class="visible content">Kirim</div>
              <div class="hidden content"><i class="paper plane outline icon"></i></div>
          	</button>
          </div>
          <div class="four wide column"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
        </div>
      </div>
    </div>

    <div id="modalEdit" class="ui small modal">
      <div class="actions">
      	<div class="ui grid">
      	  <div class="twelve wide column" style="text-align: left !important;" >
      		<div class="ui header" style="padding-top: inherit; padding-left: inherit; padding-right: inherit; text-transform: uppercase;">Ubah Ucapan</div>
      	  </div>
      	  <div class="four wide column">
        	<button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
          </div>
    	</div>
      </div>
      <div class="scrolling content">
        <div class="ui grid">
          <div class="twelve wide column">
          	<div class="modal-greetings-edit"></div>
          </div>
          <div class="four wide column"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
        </div>
      </div>
    </div>

    <div id="modalHapus" class="ui tiny basic modal">
    	<div class="content">
    		<div class="description">Data yang sudah dihapus tidak dapat dikembalikan lagi atau dengan kata lain terhapus secara permanen. Apakah Anda yakin ingin menghapus data ini?</div>
    	</div>
    	<div class="actions modal-greetings-dell"></div>
    </div>
    
    <div class="ui centered grid container">
      <div class="sixteen wide column">
        <div class="ui very padded compact segment otdc wrapper"><a href="dashboard.php"><i class="arrow circle left big icon otdc button-back"></i></a>
          <div class="ui grid">
            <div class="sixteen wide tablet eleven wide computer twelve wide large screen column">
              <h3 class="ui header">DATA UCAPAN</h3>
              <div class="ui divider"></div>
              <table id="greetingsTable" class="ui selectable basic small table otdc dataTable">
                <thead>
                  <tr>
                    <th></th>
                    <th>Pesan</th>
                    <th> </th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($dGreetings != "0") :
                  for ($i=0; $i < $gCount; $i++) : ?>
                  <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $dGreetings[$i]['greetings'] ?></td>
                    <td><span onclick="showModalEdit($(this))" data-id="<?php echo $dGreetings[$i]['greetings_id'] ?>" style="cursor: pointer !important;"><i class="cog icon"></i></span></td>
                    <td style="padding-left: 0px !important;"><span onclick="showModalHapus($(this))" data-id="<?php echo $dGreetings[$i]['greetings_id'] ?>" style="cursor: pointer !important;" class="otdc delete"><i class="trash icon"></i></span>
                    </td>
                  </tr>
              	<?php endfor; endif; ?>
                </tbody>
              </table>
            </div>
            <div class="sixteen wide tablet five wide computer four wide large screen column">
              <div class="ui search search-greetings">
                <div class="ui icon input otdc input-search">
                  <input type="text" id="searchInput" onkeyup="searchFunction()" placeholder="Pencarian ucapan." class="prompt"/>
                  <i class="search icon"></i>
                </div>
                <div class="results"></div>
              </div>
              <div onclick="showModalTambah($(this))" class="ui fluid button otdc add-patient"><i class="user plus icon"></i><span>Tambah Ucapan</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.semanticui.min.js"></script>
    <script src="assets/semantic/semantic.min.js"></script>
    <script src="assets/js/main.min.js"></script>
    <script>
    	function searchFunction() {
      	  //Declare variables
      	  var input, filter, table, tr, td, i, txtValue;
      	  input = document.getElementById("searchInput");
      	  filter = input.value.toUpperCase();
      	  table = document.getElementById("greetingsTable");
      	  tr = table.getElementsByTagName("tr");

      	  // Loop through all table rows, and hide those who don't
      	  // match the search query
      	  for ( i = 0; i < tr.length; i++) {
      	  	  td = tr[i].getElementsByTagName("td")[1];
      	  	  if (td) {
      	  	  	  txtValue = td.textContent || td.innerText;
      	  	  	  if (txtValue.toUpperCase().indexOf(filter) > -1) {
      	  	  	  	  tr[i].style.display = "";
      	  	  	  } else {
      	  	  	  	  tr[i].style.display = "none";
      	  	  	  }
      	  	  }
      	  }
      	};

    	$(document).ready(function() {
    	  // Show Modal Detail
          showModalDetail = function(_this) {
              var content = _this.data('content');
      
              $('#modalDetail .content p').html(content);
      
              $('#modalDetail').modal('show');
          }

          // Show Modal Edit
          showModalEdit = function(this_) {
              $('#modalEdit').modal('show');
              var rowid = $(this_).data('id');

              $.ajax({
              	type : 'post',
                url : 'functions/actionGreetings.php',
                data :  {
                	'g-edit-modal' : 1,
                	'rowid' : rowid,
                },
                success : function(data){
                	$('.modal-greetings-edit').html(data);
                }
              });
          }

          // Show Modal Tambah
          showModalTambah = function() {
              $('#modalTambah').modal('show');
          }

          // Show Modal Hapus
          showModalHapus = function(this_) {
              $('#modalHapus').modal('show');
              var rowid = $(this_).data('id');

              $.ajax({
              	type : 'post',
                url : 'functions/actionGreetings.php',
                data :  {
                	'g-dell-modal' : 1,
                	'rowid' : rowid,
                },
                success : function(data){
                	$('.modal-greetings-dell').html(data);
                }
              });
          }
    	})
    </script>

<?php 
  include ('footer.php');
?>