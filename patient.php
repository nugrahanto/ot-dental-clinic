<?php 
	include ('header.php');
	//check auth
	if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') {
		
	} else {
		session_destroy();
		header("location: " . BASE_URL);
	}

	$patientQRY  = "SELECT * FROM pasien ORDER BY create_date DESC";
	$allPatient  = $pdo->prepare($patientQRY);
	$allPatient->execute();

	if ($allPatient->rowCount() < 1) {
		$dPatient = "0";
	} else {
		$pCount		= $allPatient->rowCount();
		$dPatient	= $allPatient->fetchAll(PDO::FETCH_ASSOC);
	}
?>
	<style>
	  input[type=number]::-webkit-inner-spin-button,
	  input[type=number]::-webkit-outer-spin-button {
	  	-webkit-appearance: none;
	  	margin: 0;
	  }
	</style>

    <div id="modalTambah" class="ui small modal">
      <div class="actions">
        <div class="ui grid">
          <div class="twelve wide column" style="text-align: left !important;" >
      	    <div class="ui header" style="padding-top: inherit; padding-left: inherit; padding-right: inherit; text-transform: uppercase;">Tambah Pasien</div>
          </div>
          <div class="four wide column">
            <button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
          </div>
        </div>
      </div>
      <div class="scrolling content">
        <div class="ui grid">
          <div class="four wide column"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
          <div class="twelve wide column">

            <form class="ui form otdc login" method="post" id="form-pasien-add" action="functions/actionPatient.php">
              <div class="two fields">
                <div class="required field">
                  <label for="">Nama Depan</label>
                  <div class="ui transparent input">
                    <input type="text" name="firstname" required />
                  </div>
                </div>
                <div class="field">
                  <label for="">Nama Belakang</label>
                  <div class="ui transparent input">
                    <input type="text" name="lastname" />
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="required field">
                  <label for="">Tempat Lahir</label>
                  <div class="ui transparent input">
                    <input type="text" name="bplace" required />
                  </div>
                </div>
                <div class="required field">
                  <label for="">Tanggal Lahir</label>
                  <div class="ui transparent input">
                    <input type="date" name="bdate" required />
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <label for="">No. HP</label>
                  <div class="ui transparent input">
                    <input type="number" name="contact" />
                  </div>
                </div>
                <div class="required field">
                  <label for="">Jenis Kelamin</label>
                  <div class="inline fields">
                    <div class="field">
                      <div class="ui radio checkbox">
                        <input type="radio" name="gender" value="L" checked="checked" class="hidden"/>
                        <label>L</label>
                      </div>
                    </div>
                    <div class="field">
                      <div class="ui radio checkbox">
                        <input type="radio" name="gender" value="P" class="hidden"/>
                        <label>P</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="required field">
                <label for="">Alamat</label>
                <div class="ui transparent input">
                  <input type="text" name="address" />
                </div>
              </div>
              <div class="field">
                <label for="">Treatment</label>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Bleaching" class="hidden"/>
                    <label>Bleaching</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Gigi Tiruan" class="hidden"/>
                    <label>Gigi Tiruan</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Konsultasi" class="hidden"/>
                    <label>Konsultasi</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Odontectomy" class="hidden"/>
                    <label>Odontectomy</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Orthondontyl" class="hidden"/>
                    <label>Orthondontyl</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Pencabutan" class="hidden"/>
                    <label>Pencabutan</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Perawatan" class="hidden"/>
                    <label>Perawatan</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Preventif" class="hidden"/>
                    <label>Preventif</label>
                  </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Scalling" class="hidden"/>
                    <label>Scalling</label>
                  </div>
                </div>
                <div class="field">
                  <div class="ui checkbox">
                    <input type="checkbox" name="treatment[]" value="Tambal" class="hidden"/>
                    <label>Tambal</label>
                  </div>
                </div>
              </div><br/>
            </form>
            <button type="submit" form="form-pasien-add" name="pasien-add" value="Kirim" class="ui animated blue button">
              <div class="visible content">Kirim</div>
              <div class="hidden content"><i class="paper plane outline icon"></i></div>
          	</button>

          </div>
        </div>
      </div>
    </div>


    <div id="modalEdit" class="ui small modal">
      <div class="actions">
        <div class="ui grid">
          <div class="twelve wide column" style="text-align: left !important;" >
            <div class="ui header" style="padding-top: inherit; padding-left: inherit; padding-right: inherit; text-transform: uppercase;">Edit Pasien</div>
          </div>
          <div class="four wide column">
            <button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
          </div>
        </div>
      </div>
      <div class="scrolling content">
        <div class="ui grid">
          <div class="four wide column"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
          <div class="twelve wide column">

            <div class="modal-patient-edit"></div>

          </div>
        </div>
      </div>
    </div>


    <div id="modalHapus" class="ui tiny basic modal modal-patient-dell"></div>


    <div class="ui centered grid container">
      <div class="sixteen wide column">
        <div class="ui very padded compact segment otdc wrapper"><a href="dashboard.php"><i class="arrow circle left big icon otdc button-back"></i></a>
          <div class="ui grid">
            <div class="sixteen wide tablet five wide computer four wide large screen column">
              <div class="ui search search-patient">
                <div class="ui icon input otdc input-search">
                  <input type="text" id="searchInput" onkeyup="searchFunction()" placeholder="Pencarian nama pasien." class="prompt"/><i class="search icon"></i>
                </div>
                <div class="results"></div>
              </div>
              <div onclick="showModalTambah()" class="ui fluid button otdc add-patient"><i class="user plus icon"></i><span>Tambah Pasien</span></div>
            </div>
            <div class="sixteen wide tablet eleven wide computer twelve wide large screen column">
              <h3 class="ui header">DATA PASIEN</h3>
              <div class="ui divider"></div>

              <table id="patientTable" class="ui selectable basic small table otdc dataTable" style="text-transform: capitalize !important;">
                <thead>
                  <tr>
                    <th> </th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>TTL</th>
                    <th>P/L</th>
                    <th>No. HP</th>
                    <th> </th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($dPatient != "0") : for ($i=0; $i < $pCount; $i++) : ?>
                  <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $dPatient[$i]['firstname'].' '.$dPatient[$i]['lastname']; ?></td>
                    <td><?php echo $dPatient[$i]['address'] ?></td>
                    <td><?php 
                    	$date = date('d F Y', strtotime($dPatient[$i]['born_date']));
                    	echo $dPatient[$i]['born_place'].', '.$date; ?>
                    </td>
                    <td><?php echo $dPatient[$i]['gender'] ?></td>
                    <td><?php echo $dPatient[$i]['contact'] ?></td>
                    <td style="padding-right: 0px !important;"><span onclick="showModalEdit($(this))" data-id="<?php echo $dPatient[$i]['pasien_id'] ?>" style="cursor: pointer !important;"><i class="cog icon"></i></span></td>
                    <td style="padding-left: 0px !important;"><span onclick="showModalHapus($(this))" data-id="<?php echo $dPatient[$i]['pasien_id'] ?>" style="cursor: pointer !important;" class="otdc delete"><i class="trash icon"></i></span>
                    </td>
                  </tr>
                <?php endfor; endif; ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.semanticui.min.js"></script>
    <script src="assets/semantic/semantic.min.js"></script>
    <script src="assets/js/main.min.js"></script>
    <script>
      function searchFunction() {
      	  //Declare variables
      	  var input, filter, table, tr, td, i, txtValue;
      	  input = document.getElementById("searchInput");
      	  filter = input.value.toUpperCase();
      	  table = document.getElementById("patientTable");
      	  tr = table.getElementsByTagName("tr");

      	  // Loop through all table rows, and hide those who don't
      	  // match the search query
      	  for ( i = 0; i < tr.length; i++) {
      	  	  td = tr[i].getElementsByTagName("td")[1];
      	  	  if (td) {
      	  	  	  txtValue = td.textContent || td.innerText;
      	  	  	  if (txtValue.toUpperCase().indexOf(filter) > -1) {
      	  	  	  	  tr[i].style.display = "";
      	  	  	  } else {
      	  	  	  	  tr[i].style.display = "none";
      	  	  	  }
      	  	  }
      	  }
      };

      $(document).ready(function() {
          // Show Modal Tambah
          showModalTambah = function() {
              $('#modalTambah').modal('show');
          }
      
          // Show Modal Edit
          showModalEdit = function(this_) {
              $('#modalEdit').modal('show');
              var rowid = $(this_).data('id');

              $.ajax({
              	type : 'post',
                url : 'functions/actionPatient.php',
                data :  {
                	'p-edit-modal' : 1,
                	'rowid' : rowid,
                },
                success : function(data){
                	$('.modal-patient-edit').html(data);
                }
              });
          }
      
          // Show Modal Hapus
          showModalHapus = function(this_) {
              $('#modalHapus').modal('show');
              var rowid = $(this_).data('id');

              $.ajax({
              	type : 'post',
                url : 'functions/actionPatient.php',
                data :  {
                	'p-dell-modal' : 1,
                	'rowid' : rowid,
                },
                success : function(data){
                	$('.modal-patient-dell').html(data);
                }
              });
          }
      })
    </script>
    
<?php 
  include ('footer.php');
?>