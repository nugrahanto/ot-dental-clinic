<?php
  error_reporting(0);
  session_start();
  include("./functions/koneksi_db.php");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>OT Dental Clinic</title>
    <link rel="stylesheet" href="assets/semantic/semantic.min.css"/>
    <link rel="stylesheet" href="assets/css/style.min.css"/>

    <!-- Base website -->
    <base href="<?php echo BASE_URL; ?>">
  </head>
  <body>