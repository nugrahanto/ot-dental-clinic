<?php include ('header.php'); ?>

    <div class="ui centered grid container">
      <div class="sixteen wide mobile ten wide tablet eight wide computer seven wide large screen column">
        <div class="ui very padded basic segment"><img src="assets/images/logo.png" alt="" class="ui tiny circular centered image"/>
          <div class="otdc mt50"></div>
          <h4 class="ui center aligned header">SIGN UP</h4>
          <form action="functions/actionLogin.php" method="post" id="form-signup" class="ui form otdc login">
            <div class="required field">
              <label for="">First Name</label>
              <div class="ui transparent input">
                <input type="text" name="firstname" required/>
              </div>
            </div>
            <div class="required field">
              <label for="">Last Name</label>
              <div class="ui transparent input">
                <input type="text" name="lastname" />
              </div>
            </div>
            <div class="required field">
              <label for="">Username</label>
              <div class="ui transparent input">
                <input type="text" name="username" required/>
              </div>
            </div>
            <div class="required field">
              <label for="">Password</label>
              <div class="ui transparent input">
                <input type="password" name="password" required/>
              </div>
            </div>
            <div class="ui center aligned basic segment"><a href="<?php echo BASE_URL;?>" >
                <button type="button" class="ui animated button">
                  <div class="visible content">Cancel</div>
                  <div class="hidden content"><i class="left arrow icon"></i></div>
                </button></a>
              <button type="submit" form="form-signup" name="user-signup" value="save" class="ui animated blue button">
                <div class="visible content">Save</div>
                <div class="hidden content"><i class="save icon"></i></div>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>

<?php include ('footer.php'); ?>