<?php 
	include ('header.php');
	//check auth
	if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') {
		
	} else {
		session_destroy();
		header("location: " . BASE_URL);
	}
?>
	<div id="modalDetail" class="ui tiny modal">
      <div class="content otdc item-notification">
        <div class="ui grid">
          <div class="sixteen wide mobile four wide tablet four wide computer four wide large screen center aligned column otdc icon-notification"><i class="huge gift icon"></i></div>
          <div class="sixteen wide mobile twelve wide tablet twelve wide computer twelve wide large screen column">
            <button onclick="hideModalDetail()" class="circular ui cancel icon small right floated button otdc close-modal modal-calendar" style="z-index: 99;"><i class="close icon"></i></button>
            <div id="modalContent" class="ui list" style="margin: 0 !important;"></div>
          </div>
        </div>
      </div>
    </div>

    <div id="modalProfil" class="ui small modal">
    	<div class="actions">
    		<div class="ui grid">
    			<div class="twelve wide column" style="text-align: left !important;" >
    				<div class="ui header" style="padding-top: inherit; padding-left: inherit; padding-right: inherit; text-transform: uppercase;">Profil Pasien</div>
    			</div>
    			<div class="four wide column">
    				<button class="circular ui cancel icon small button otdc close-modal"><i class="close icon"></i></button>
    			</div>
    		</div>
    	</div>
    	<div class="scrolling content">
    		<div class="ui grid">
    			<div class="four wide column"><img src="assets/images/logo.png" alt="" class="ui small circular centered image"/></div>
    			<div class="twelve wide column">
    				<div class="patient-profil"></div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="ui centered grid container">
    	<div class="sixteen wide column">
    		<div class="ui very padded compact segment otdc wrapper"><a href="dashboard.php"><i class="arrow circle left big icon otdc button-back"></i></a>
    			<div class="ui centered grid">
    				<div class="sixteen wide center aligned column">
    					<h3 class="ui header">KALENDER</h3>
    				</div>
    					
    					<div id="calender_section" >
    						<div class="ui equal width grid" style="text-align: center; vertical-align: middle;">
    							<div class="column"><i onclick="previous()" class="angle double left large icon" style="padding: inherit; cursor: pointer;"></i></div>
    							<div class="column"><h2 class="card-header" id="monthAndYear"></h2></div>
    							<div class="column"><i onclick="next()" class="angle double right large icon" style="padding: inherit; cursor: pointer;"></i></div>
    						</div>

    						<div id="calender_section_top">
    							<ul>
    								<li>Sun</li>
    								<li>Mon</li>
    								<li>Tue</li>
    								<li>Wed</li>
    								<li>Thu</li>
    								<li>Fri</li>
    								<li>Sat</li>
    							</ul>
    						</div>
    						<div id="calender_section_bot"></div>
    						<br/>
    						<div id="calender_section_foot">
    						<form class="form-inline" style="text-align: left;">
    							<label for="month" style="margin-right: 10px;">Jump To: </label>
    							<select class="selection ui dropdown" name="month" id="month" onchange="jump()">
    								<option value=0>Januari</option>
    								<option value=1>Februari</option>
    								<option value=2>Maret</option>
    								<option value=3>April</option>
    								<option value=4>Mei</option>
    								<option value=5>Juni</option>
    								<option value=6>Juli</option>
    								<option value=7>Agustus</option>
    								<option value=8>September</option>
    								<option value=9>Oktober</option>
    								<option value=10>November</option>
    								<option value=11>Desember</option>
    							</select>
    						</form>
    						</div>
    					</div>

    			</div>
    		</div>
    	</div>
    </div>

	
	<script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/dataTables.semanticui.min.js"></script>
	<script src="assets/semantic/semantic.min.js"></script>
	<script src="assets/js/main.min.js"></script>
	<script>
		var today = new Date();
		var month = today.getMonth();
		var year = today.getFullYear();
		var currentMonth = month;
		var currentYear = year;
		var selectMonth = document.getElementById("month");

		function loadHeader(month, year) {
			var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
			
			monthAndYear = document.getElementById("monthAndYear");
			monthAndYear.innerHTML = months[month] +" "+ year;
		}

		function renderCalendar(month, year){
			var firstDay = (new Date(year, month)).getDay();
			var daysInMonth = 32 - new Date(year, month, 32).getDate();
			var calendar = document.getElementById("calender_section_bot");
			var totalDaysDisplay = (firstDay == 7)?(daysInMonth):(daysInMonth + firstDay);
			var boxDisplay = (totalDaysDisplay <= 35)?35:42;

			// clearing all previous cells
			calendar.innerHTML = "";

			selectMonth.value = month;

			//creating all cells
			$.ajax({
				type : 'post',
				url : 'functions/actionCalendar.php',
				data :  {
					'get-calendar' 	: 1,
					'firstday'		: firstDay,
					'dayinmonth'	: daysInMonth,
					'boxdisplay'	: boxDisplay,
					'totaldaysdisplay' : totalDaysDisplay,
					'month'			: month,
					'year'			: year,
					'todayDate'		: today.getDate(),
					'todayMonth'	: today.getMonth(),
					'todayYear'		: today.getFullYear()
				},
				success : function(data){
					document.getElementById('calender_section_bot').innerHTML = data;
				}
			});
		}

		function next() {
			currentMonth = (currentMonth + 1) % 12;
			loadHeader(currentMonth, currentYear);
			renderCalendar(currentMonth, currentYear);
		}

		function previous() {
			currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
			loadHeader(currentMonth, currentYear);
			renderCalendar(currentMonth, currentYear);
		}

		function jump() {
			currentMonth = parseInt(selectMonth.value);
			loadHeader(currentMonth, currentYear);
			renderCalendar(currentMonth, currentYear);
		}

		$(document).ready(function(){
			loadHeader(month, year);
			renderCalendar(month, year);

			// Show Modal Detail
			showModalDetail = function(dataDate) {
				var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
				var mDate = dataDate+"";
				mDate = mDate.split(".");
				mDate[1] = months[mDate[1]];
				liClass = ".li-"+mDate[0];
				dataId = $(liClass).data('rowid');

				$.ajax({
					type : 'post',
					url : 'functions/actionCalendar.php',
					data :  {
						'get-sub-calendar' : 1,
						'id' : dataId,
						'month' : mDate[1],
						'day' : mDate[0]
					},
					success : function(data){
						$('#modalContent').html(data);
					}
				});
				$('#modalDetail').modal('show');
			}

			// Hide Modal Detail
			hideModalDetail = function() {
				$('#modalDetail').modal('hide');
			}

			// Show Modal Profil
    		showModalProfil = function(id) {
				$('#modalDetail').modal('hide');
    			$('#modalProfil').modal('show');
    			var pasid = id;
    			$.ajax({
    				type : 'post',
    				url : 'functions/actionCalendar.php',
    				data :  {
    					'get-patient-profil' : 1,
    					'pasid' : pasid,
    				},
    				success : function(data){
    					$('.patient-profil').html(data);
    				}
    			});
    		}
		})
	</script>
<?php
	include ('footer.php');
?>
